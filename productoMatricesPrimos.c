
#include <stdio.h>
#include <stdlib.h>
//AVALOS SERRANO CHRISTIAN VLADIMIR AS16018


int main() {
    
    int filas, columnas, fila, columna,sumaPrimos=0;
    printf("Ingrese las filas de la matriz\n");
    scanf("%d", &filas);
    printf("Ingrese las columnas de la matriz\n");
    scanf("%d", &columnas);
    
    int A[filas][columnas];
    int B[filas][columnas];
    int C[filas][columnas];
    
    for(fila=0; fila<filas; fila++){
        for(columna=0; columna<columnas; columna++){
            
            printf("ingrese valores de la matriz A %d%d \n", fila+1, columna+1);
            scanf("%d", &A[fila][columna]);
        }
    }
    for(fila=0; fila<filas; fila++){
        for(columna=0; columna<columnas; columna++){
            
            printf("ingrese valores de la matriz B %d%d \n", fila+1, columna+1);
            scanf("%d", &B[fila][columna]);
        }
    }
    
    //Multiplicacion de ambas matrices
    for(fila=0; fila<filas; fila++){
        for(columna=0; columna<columnas; columna++){
            C[fila][columna]=A[fila][columna]*B[columna][fila];
            
        }
    }
    
    printf("El producto es :\n");
    for(fila=0; fila<filas; fila++){

        for(columna=0; columna<columnas; columna++){
            printf("[%d]",C[fila][columna] );   
        }
        printf("\n");   
    }
    
    //Determinar si un numero es primo
    printf("los numeros primos de la matriz son:\n");
    for(fila=0; fila<filas; fila++){
        for(columna=0; columna<columnas; columna++){
            int contador=0;
            for(int i=1; i<=C[fila][columna]; i++){
                if(((C[fila][columna]) % i)==0){
                    contador++;
                }
            }
            if(contador==2){
                printf("#%d en la posicion [%d,%d]\n",C[fila][columna],fila,columna);
                sumaPrimos=sumaPrimos+1;
            }
            
        }
    }
    
    printf("El tamaño del vector es : %d",sumaPrimos);
    int vector[sumaPrimos];
    int posicion=0;
    
    for(fila=0; fila<filas; fila++){
        for(columna=0; columna<columnas; columna++){
            int contador=0;
            for(int i=1; i<=C[fila][columna]; i++){
                if(((C[fila][columna]) % i)==0){
                    contador++;
                }
            }
            if(contador==2){
                vector[posicion]=C[fila][columna];
                posicion=posicion+1;
            }
            
        }
    }
    //Mostramos el vector de una vez ya ordenado
    int orden;
    printf("\nEl vector es:\n");
    for(int i=0; i<sumaPrimos; i++){
        if (vector[i] > vector[i+1])
      {
        orden= vector[i];
        vector[i]=vector[i+1];
        vector[i+1]=orden;
      }
        printf("[%d]",vector[i]);
    }
    
            

    return 0;
}

